package main

import (
    "bufio"
    "fmt"
    "os"
    "reflect"
    "strconv"
    "strings"
)

var ParkingLotInstance *ParkingLot

var funcMap = map[string]Function{
    "create_parking_lot": Function{CommandCreateParkingLot, 1, "<number_of_slots>"},
    "park": Function{CommandPark, 2, "<registration_number> <color>"},
    "leave": Function{CommandLeave, 1, "<slot_number>"},
    "status": Function{CommandStatus, 0, ""},
    "registration_numbers_for_cars_with_colour": Function{CommandRegistrationNumbersForCarsWithColour, 1, "<color>"},
    "slot_numbers_for_cars_with_colour": Function{CommandSlotNumbersForCarsWithColour, 1, "<color>"},
    "slot_number_for_registration_number": Function{CommandSlotNumberForRegistrationNumber, 1, "<registration_number>"},
}

type Function struct {
    name interface{}
    argLen int
    format string
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func noParrkingLotMessage() string {
    return "No parking lot. To create one, just type : create_parking_lot <number_of_lot>."
}

func CommandCreateParkingLot(strNumber string) (string) {
    number, _ := strconv.Atoi(strNumber)
    p, err := CreateParkingLot(number)
    if err != nil {
        return "Please provide any number starting from 1"
    } else {
        ParkingLotInstance = p
        return "Created a parking lot with "+strconv.Itoa(number)+" slots"
    }
}

func CommandPark(plate string, color string) string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    result, err := ParkingLotInstance.CreateTicket(plate, color)
    if result != nil && err == nil {
        return "Allocated slot number: " + strconv.Itoa(result.slot)
    } else {
        return fmt.Sprint(err)
    }
}

func CommandLeave(strNumber string) string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    number, _ := strconv.Atoi(strNumber)
    ok, err := ParkingLotInstance.Leave(number)
    if ok {
        return "Slot number " + strNumber + " is free"
    } else {
        return fmt.Sprint(err)
    }
}

func CommandStatus() string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    return ParkingLotInstance.Status()
}

func CommandRegistrationNumbersForCarsWithColour(color string) string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    slots, err := ParkingLotInstance.FindSlotsByColor(color)
    if err != nil {
        return fmt.Sprint(err)
    } else {
        var plates []string
        for _, ticket := range slots {
            plates = append(plates, ticket.plate)
        }
        return strings.Join(plates, ", ")
    }
}

func CommandSlotNumbersForCarsWithColour(color string) string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    slots, err := ParkingLotInstance.FindSlotsByColor(color)
    if err != nil {
        return fmt.Sprint(err)
    } else {
        var slotNumbers []string
        for slotNumber, _ := range slots {
            slotNumbers = append(slotNumbers, strconv.Itoa(slotNumber))
        }
        return strings.Join(slotNumbers, ", ")
    }
}

func CommandSlotNumberForRegistrationNumber(plate string) string {
    if ParkingLotInstance == nil {
        return noParrkingLotMessage()
    }

    slot, err := ParkingLotInstance.FindSlotByPlate(plate)
    if err != nil {
        return fmt.Sprint(err)
    } else {
        return strconv.Itoa(slot)
    }
}

func RunCommand(cmd string) (error) {
    args := strings.Split(cmd, " ")
    command := args[0]
    params := args[1:]
    if function, exist := funcMap[command]; exist {
        if len(params) != function.argLen {
            return fmt.Errorf("Wrong format. Type : parking_lot " + function.format)
        }
        f := reflect.ValueOf(function.name)
        in := make([]reflect.Value, len(params))
        for k, param := range params {
            in[k] = reflect.ValueOf(param)
        }
        fmt.Println(f.Call(in)[0])
    } else {
        fmt.Println("\nNot identified command.")
        fmt.Println("\nList of commands :")
        fmt.Println("===================")
        for c, a := range funcMap {
            fmt.Println(c + " "+a.format)
        }
    }
    return nil
}

func readFile(filename string) (bool, error) {
    if fileExists(filename) {
        file, err := os.Open(filename)
        check(err)

        defer file.Close()

        scanner := bufio.NewScanner(file)
        for scanner.Scan() {
            err = RunCommand(scanner.Text())
            if err != nil {
                return false, err
            }
        }
        return true, nil
    } else {
        return false, fmt.Errorf("File not found "+filename)
    }
}

func fileExists(filename string) bool {
    info, err := os.Stat(filename)
    if os.IsNotExist(err) {
        return false
    }
    return !info.IsDir()
}

func interactive() {
    var strInput string
    scanner := bufio.NewScanner(os.Stdin)
    if scanner.Scan() {
        strInput = scanner.Text()

    }
    if (strInput == "exit") {
        os.Exit(0)
    } else {
        RunCommand(strInput)
        fmt.Println("\n ")
        interactive()
    }
}

func main() {
    args := os.Args[1:]

    if len(args) == 0 {
        interactive()
        return;
    }

    arg := args[0]
    switch arg {
    case "--help":
        fmt.Println("type : parking_lot --test <file_location>")
    case "--test":
        if len(args) > 1 {
            file := args[1]
            _, err := readFile(file)
            if err != nil {
                fmt.Println(err)
            }
        } else {
            fmt.Println("No file")
        }
    }
}

