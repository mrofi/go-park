package main

import (
    "fmt"
    "math/rand"
    "testing"
    "reflect"
    "strconv"
    "strings"
)

var testName string

type Car struct {
    plate string
    color string
}

func CreatingParkingLotWithCars(number int) (*ParkingLot, map[int]Car) {
    parkingLot, _ := CreateParkingLot(number)

    cars := make(map[int]Car)
    cars[1] = Car{"KA-01-HH-1234", "White"}
    cars[2] = Car{"KA-01-HH-9999", "White"}
    cars[3] = Car{"KA-01-BB-0001", "Black"}
    cars[4] = Car{"KA-01-HH-2701", "Blue"}
    cars[5] = Car{"KA-01-HH-3141", "Black"}
    cars[6] = Car{"KA-01-BB-7877", "Red"}

    return parkingLot, cars
}

func TestCreateParkingLot(t *testing.T) {

    fmt.Println("TestCreateParkingLot")

    testName = "\n1. Return must be *main.ParkingLot type :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        number := 10
        created, _ := CreateParkingLot(number)
        typeOfCreated := reflect.TypeOf(created).String()
        if typeOfCreated == "*main.ParkingLot" {
            fmt.Printf("OK. This type is %v \n", typeOfCreated)
        } else {
            t.Errorf("Error. This type is %v", typeOfCreated)
        }
    })

    testName = "\n2. No parking created when parameter is integer 0 :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        number := 0
        created, err := CreateParkingLot(number)
        if created == nil && err != nil {
            fmt.Println(err)
        } else {
            t.Error("Parking is created and no error")
        }
    })

    testName = "\n3. Parking created when parameter is integer starting from 1 :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        for number := 1; number < 3; number++ {
            testNumber := number * number + number
            fmt.Printf("Test create %v parking lots \n", testNumber)
            created, err := CreateParkingLot(testNumber)
            if created == nil && err != nil {
                t.Error("Can't create parking lot")
            } else {
                fmt.Printf("Created %v \n", created)
            }
        }
    })
}

func TestCreateTicket(t *testing.T) {

    fmt.Println("\nTestCreateTicket")
    fmt.Println("Create Parking Lot (5 lots)")
    
    parkingLot, cars := CreatingParkingLotWithCars(5)
    
    testName = "\n1. Create Ticket for 5 cars :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        for i := 1; i <= 5; i++ {
            car := cars[i]
            ticket, err := parkingLot.CreateTicket(car.plate, car.color)
            if ticket != nil && err == nil {
                fmt.Printf("Allocated slot : %v \n", ticket.slot)
            } else {
                t.Error(err)
            }
        }
    })

    testName = "\n2. Creating Ticket for 6th car must be declined :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        car := cars[6]
        ticket, err := parkingLot.CreateTicket(car.plate, car.color)
        if ticket == nil && err != nil {
            fmt.Println(err)
        } else {
            t.Error("Ticket is created")
        }
    })
}

func TestLeaveLot(t *testing.T) {

    parkingLot, cars := CreatingParkingLotWithCars(5)
    for i := 1; i <= 5; i++ {
        car := cars[i]
        parkingLot.CreateTicket(car.plate, car.color)
    }
    fmt.Println("\nTestLeaveLot")
    testName = "\n1. Car leaves from lot 4 :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        free, err := parkingLot.Leave(4)
        if free && err == nil {
            fmt.Println("Slot number 4 is free")
        } else {
            t.Error(err)
        }
    })
}

func TestStatus(t *testing.T) {
    parkingLot, cars := CreatingParkingLotWithCars(5)
    for i := 1; i <= 5; i++ {
        car := cars[i]
        parkingLot.CreateTicket(car.plate, car.color)
    }
    parkingLot.Leave(4)
    fmt.Println("\nTestStatus")
    testName = "\n1. Print Status :"
    t.Run(testName, func (t *testing.T) {
        status := parkingLot.Status()
        if ! (strings.Contains(status, "KA-01-HH-2701") && strings.Contains(status, "Blue")) {
            fmt.Println(status)
        } else {
            t.Error("No status shown")
        }
    })
}

func TestFindSlotsByColor(t *testing.T) {

    colors := make(map[string]int)

    parkingLot, cars := CreatingParkingLotWithCars(6)
    for i := 1; i <= 6; i++ {
        car := cars[i]
        if _, exist := colors[car.color]; exist {
            colors[car.color]++
        } else {
            colors[car.color] = 1
        }
        parkingLot.CreateTicket(car.plate, car.color)
    }

    fmt.Println("\nTestFindSlotsByColor")
    fmt.Println("Recorded number of cars that have color :")
    fmt.Println(colors)

    color := "White"
    slots, _ := parkingLot.FindSlotsByColor(color)
    testName = "\n1. Get number of cars that has color " + color + " :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        if number := len(slots); number == colors[color] {
            fmt.Printf("%v cars founded has color %v\n", number, color)
        } else {
            t.Error("Founded cars data is not valid.")
        }
    })

    testName = "\n2. Show registration number of cars that have color " + color + " :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        if number := len(slots); number == colors[color] {
            var plates []string
            for _, ticket := range slots {
                plates = append(plates, ticket.plate)
            }
            fmt.Println(strings.Join(plates, ", "))
        } else {
            t.Error("Founded cars data is not valid.")
        }
    })

    testName = "\n3. Show slot number of cars that have color " + color + " :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        if number := len(slots); number == colors[color] {
            var slotNumbers []string
            for slotNumber, _ := range slots {
                slotNumbers = append(slotNumbers, strconv.Itoa(slotNumber))
            }
            fmt.Println(strings.Join(slotNumbers, ", "))
        } else {
            t.Error("Founded cars data is not valid.")
        }
    })

    color = "Rainbow"
    testName = "\n4. Find slot that color is not exist \""+ color +"\" :"
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        slots, err := parkingLot.FindSlotsByColor(color)
        if slots == nil && err != nil {
            fmt.Println(err)
        } else {
            t.Error("Founded cars data is not valid.")
        }
    })
}

func TestFindSlotByPlate(t *testing.T) {
    fmt.Println("\nTestFindSlotByPlate")

    parkingLot, cars := CreatingParkingLotWithCars(6)
    plates := make(map[int]string)
    for i := 1; i <= 6; i++ {
        car := cars[i]
        ticket, _ := parkingLot.CreateTicket(car.plate, car.color)
        plates[ticket.slot] = ticket.plate
    }

    fmt.Println("Print current status :")
    fmt.Println(parkingLot.Status())

    for i := 1; i <= 6; i++ {
        randomSlot := rand.Intn(5)
        plate := plates[randomSlot]
        testName = "\n"+ strconv.Itoa(i) +". Find slot for car with registration number  " + plate + " : "
        t.Run(testName, func (t *testing.T) {
            fmt.Print(testName)
            slot, err := parkingLot.FindSlotByPlate(plate)
            if slot == 0 && err != nil {
                fmt.Println(err)
            } else {
                if slot == randomSlot {
                    fmt.Println(slot)
                } else {
                    t.Error("Wrong slot")
                }
            }
        })
    }

    plate := "XX-000-XX"
    testName = "\n7. Find slot that registration number is not listed \""+ plate +"\" : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        slot, err := parkingLot.FindSlotByPlate(plate)
        if slot == 0 && err != nil {
            fmt.Println(err)
        } else {
            t.Error("Return slot is not valid.")
        }
    })
}
