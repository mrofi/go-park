# Parking Lot

## Instalation
This command will remove previous version of parking_lot app in your GOPATH and replace with the new one.
```bash
bin/setup
```

## How To Use
```bash
bin/parking_lot
```

## Running Test
```bash
bin/run_functional_tests
```

## About
Simple Parking Lot App 
written in Golang. 
By Mokhamad Rofiudin <mokh.rofiudin@gmail.com>

