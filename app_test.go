package main

import (
    "fmt"
    "strings"
    "strconv"
    "testing"
)

func TestCommandCreateParkingLot(t *testing.T) {
    fmt.Println("\n#Test Run CommandCreateParkingLot")

    testName = "\n1. Test if argument is 0 : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandCreateParkingLot("0")
        if result == "Please provide any number starting from 1" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n2. Test if argument is 6 : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandCreateParkingLot("6")
        if result == "Created a parking lot with 6 slots" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandPark(t *testing.T) {
    fmt.Println("\n#Test Run CommandPark")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandPark("KA-01-HH-1234", "White")
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n2. Test Park a car after creating a Parking Lot : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandCreateParkingLot("6")
        result := CommandPark("KA-01-HH-1234", "White")
        if result == "Allocated slot number: 1" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test Park a car but slot has been full : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandCreateParkingLot("2")
        CommandPark("KA-01-HH-1234", "White")
        CommandPark("KA-01-HH-9999", "White")
        result := CommandPark("KA-01-BB-0001", "Black")
        if result == "Sorry, parking lot is full" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandLeave(t *testing.T) {
    fmt.Println("\n#Test Run CommandLeave")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandLeave("4")
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    CommandCreateParkingLot("6")
    CommandPark("KA-01-HH-1234", "White")
    CommandPark("KA-01-HH-9999", "White")
    CommandPark("KA-01-BB-0001", "Black")
    CommandPark("KA-01-HH-7777", "Red")
    CommandPark("KA-01-HH-2701", "Blue")
    CommandPark("KA-01-HH-3141", "Black")

    testName = "\n2. Test Leave a car after creating a Parking Lot has 6 cars in : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandLeave("4")
        if result == "Slot number 4 is free" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test Leave a car but slot has been freed in previous : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandLeave("4")
        if result == "Sorry, slot number 4 has been freed" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandStatus(t *testing.T) {
    fmt.Println("\n#Test Run CommandStatus")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandStatus()
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    CommandCreateParkingLot("6")
    CommandPark("KA-01-HH-1234", "White")
    CommandPark("KA-01-HH-9999", "White")
    CommandPark("KA-01-BB-0001", "Black")
    CommandPark("KA-01-HH-7777", "Red")
    CommandPark("KA-01-HH-2701", "Blue")
    CommandPark("KA-01-HH-3141", "Black")

    testName = "\n2. Test Status after creating a Parking Lot has 6 cars in : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandStatus()
        if strings.Contains(result, "KA-01-HH-1234") && strings.Contains(result, "KA-01-HH-7777") && strings.Contains(result, "Red") {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test Status but slot number 4  has been freed : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandLeave("4")
        result := CommandStatus()
        if ! (strings.Contains(result, "KA-01-HH-7777") && strings.Contains(result, "Red")) {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandRegistrationNumbersForCarsWithColour(t *testing.T) {
    fmt.Println("\n#Test Run CommandRegistrationNumbersForCarsWithColour")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandRegistrationNumbersForCarsWithColour("White")
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    CommandCreateParkingLot("6")
    CommandPark("KA-01-HH-1234", "White")
    CommandPark("KA-01-HH-9999", "White")
    CommandPark("KA-01-BB-0001", "Black")
    CommandPark("KA-01-HH-2701", "Blue")
    CommandPark("KA-01-HH-3141", "Black")
    CommandPark("KA-01-P-333", "White")

    testName = "\n2. Test find car plate numbers with color White after creating a Parking Lot has 6 cars in : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandRegistrationNumbersForCarsWithColour("White")
        if strings.Contains(result, "KA-01-HH-1234") && strings.Contains(result, "KA-01-HH-9999") && strings.Contains(result, "KA-01-P-333") {
            fmt.Println("OK")
        } else {
            fmt.Println(result)
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test find car plate numbers with color Maroon : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandLeave("4")
        result := CommandRegistrationNumbersForCarsWithColour("Maroon")
        if result == "Not found" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandSlotNumbersForCarsWithColour(t *testing.T) {
    fmt.Println("\n#Test Run CommandSlotNumbersForCarsWithColour")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandSlotNumbersForCarsWithColour("White")
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    CommandCreateParkingLot("6")
    CommandPark("KA-01-HH-1234", "White")
    CommandPark("KA-01-HH-9999", "White")
    CommandPark("KA-01-BB-0001", "Black")
    CommandPark("KA-01-P-333", "White")
    CommandPark("KA-01-HH-7777", "Red")
    CommandPark("KA-01-HH-2701", "Blue")

    testName = "\n2. Test find slot numbers with color White after creating a Parking Lot has 6 cars in : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandSlotNumbersForCarsWithColour("White")
        if strings.Contains(result, "1") && strings.Contains(result, "2") && strings.Contains(result, "4") {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test find slot numbers with color Maroon : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandLeave("4")
        result := CommandSlotNumbersForCarsWithColour("Maroon")
        if result == "Not found" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestCommandSlotNumberForRegistrationNumber(t *testing.T) {
    fmt.Println("\n#Test Run CommandSlotNumberForRegistrationNumber")

    testName = "\n1. Test if argument is parking lot not created yet : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        ParkingLotInstance = nil
        result := CommandSlotNumberForRegistrationNumber("KA-01-HH-3141")
        if result == "No parking lot. To create one, just type : create_parking_lot <number_of_lot>." {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    CommandCreateParkingLot("6")
    CommandPark("KA-01-HH-1234", "White")
    CommandPark("KA-01-HH-9999", "White")
    CommandPark("KA-01-BB-0001", "Black")
    CommandPark("KA-01-HH-7777", "Red")
    CommandPark("KA-01-HH-2701", "Blue")
    CommandPark("KA-01-HH-3141", "Black")

    testName = "\n2. Test find slot number with plate number KA-01-HH-3141 after creating a Parking Lot has 6 cars in : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        result := CommandSlotNumberForRegistrationNumber("KA-01-HH-3141")
        if result == "6" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })

    testName = "\n3. Test find slot number with plate number MH-04-AY-1111 : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        CommandLeave("4")
        result := CommandSlotNumberForRegistrationNumber("MH-04-AY-1111")
        if result == "Not found" {
            fmt.Println("OK")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestReadFile(t *testing.T) {
    fmt.Println("\n#Test Read File")

    testName = "\n1. Test Error when file not found : "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        _, err := readFile("/tmp/xxxxxxx")
        if err != nil {
            fmt.Println(err)
        } else {
            t.Error("Not OK")
        }
    })

    filename := "test/file_input.txt"
    testName = "\n2. Test Read file : " + filename
    t.Run(testName, func (t *testing.T) {
        fmt.Println(testName)
        fmt.Println("Get result :")
        ok, _ := readFile(filename)
        if ok {
            fmt.Println("File End.")
        } else {
            t.Error("Not OK")
        }
    })
}

func TestRunCommand(t *testing.T) {
    fmt.Println("\n#Test Run Command")

    n := 1
    for name, function := range funcMap {
        testName = "\n"+strconv.Itoa(n)+". Test "+name+": "
        t.Run(testName, func (t *testing.T) {
            fmt.Print(testName)
            var strParams string
            if function.argLen == 0 {
                strParams = ""
            } else if function.argLen == 1 {
                strParams = " Blue"
            } else if function.argLen == 2 {
                strParams = " KA-01-HH-2701 Blue"
            }

            err := RunCommand(name + strParams)
            if err == nil {
                fmt.Println("No error")
            } else {
                t.Error(err)
            }
        })
        n++
    }

    for name, function := range funcMap {
        testName = "\n"+strconv.Itoa(n)+". Test Invalid Format "+name+": "
        t.Run(testName, func (t *testing.T) {
            fmt.Print(testName)
            var strParams string
            if function.argLen == 1 {
                strParams = ""
            } else if function.argLen == 2 {
                strParams = " Blue"
            } else if function.argLen == 0 {
                strParams = " KA-01-HH-2701 Blue"
            }

            err := RunCommand(name + strParams)
            if err != nil {
                fmt.Println(err)
            } else {
                t.Error("Not OK")
            }
        })
        n++
    }

    name := "xxxxxxx"
    testName = "\n"+strconv.Itoa(n)+". Test No Command "+name+": "
    t.Run(testName, func (t *testing.T) {
        fmt.Print(testName)
        err := RunCommand(name)
        if err == nil {
            fmt.Println("No error")
        } else {
            t.Error(err)
        }
    })
    fmt.Println("\n Next Test")
}

